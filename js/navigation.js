/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
( function( $ ) {

	var main_navigation = $( '.main-navigation' );

	function menu_responsive() {
		$( '.main-navigation .page_item_has_children > a, .main-navigation .menu-item-has-children > a' ).addClass( 'dropdown-link' );
	}

	function menu_toggles() {

		$( '.menu-toggle' ).click( function( e ) {

			e.preventDefault();
			if ( main_navigation.hasClass( 'toggled' ) ) {
				$( this ).attr( 'aria-expanded', 'false' );
				main_navigation.removeClass( 'toggled' );
			} else {
				$( this ).attr( 'aria-expanded', 'true' );
				main_navigation.addClass( 'toggled' );
			}

		} );

		$( '.dropdown-toggle' ).click( function( e ) {

			e.preventDefault();
			if ( $( this ).hasClass( 'toggle-on' ) ) {
				$( this ).attr( 'aria-expanded', 'false' );
				$( this ).removeClass( 'toggle-on' );
				$( this ).parent( 'a' ).removeClass( 'toggle-on' );
				$( this ).parent().next( '.sub-menu' ).removeClass( 'toggle-on' );
			} else {
				$( this ).attr( 'aria-expanded', 'true' );
				$( this ).addClass( 'toggle-on' );
				$( this ).parent( 'a' ).addClass( 'toggle-on' );
				$( this ).parent().next( '.sub-menu' ).addClass( 'toggle-on' );
			}

		} );

	}

	function sequential_menu() {

		menu_responsive();
		menu_toggles();

	}

	$( document ).ready( sequential_menu );
	$( window ).resize( _.debounce( menu_responsive, 100 ) );

} )( jQuery );
